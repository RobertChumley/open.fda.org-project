/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    contentSecurityPolicy: {
      'default-src': "'none'",
      'script-src': "'self' 'unsafe-inline' 'unsafe-eval' open.fda.gov connect.facebook.net maps.googleapis.com maps.gstatic.com maxcdn.bootstrapcdn.com cdnjs.cloudflare.com code.jquery.com cdn.datatables.net",
      'font-src': "'self' data: api.fda.gov maxcdn.bootstrapcdn.com cdnjs.cloudflare.com code.jquery.com", 
      'connect-src': "'self' api.fda.gov maxcdn.bootstrapcdn.com cdnjs.cloudflare.com code.jquery.com",
      'img-src': "'self' www.facebook.com p.typekit.net",
      'style-src': "'self' 'unsafe-inline' api.fda.gov maxcdn.bootstrapcdn.com cdnjs.cloudflare.com code.jquery.com",
      'frame-src': "s-static.ak.facebook.com static.ak.facebook.com www.facebook.com"
    },
    modulePrefix: 'finance-app',
    environment: environment,
    baseURL: '/',
    locationType: 'auto', 
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
