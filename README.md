# FDA Open Data Search Engine for GSA 18F RFQ
## Pool 2- Development
## Team Radus Software- Technical Approach

![alt tag](https://bytebucket.org/RobertChumley/open.fda.org-project/raw/eb79e64ea8ead29797bd960e9d5bc69c834ced2b/public/arrow-image.png)

[Link to working development prototype](http://fdacustompilot.azurewebsites.net/)


We used a process of rapid prototypes and extreme programming to build this system.  We accomplished this by using micro-prototypes which exercised the API and allowed us to 
form the foundation to the backend system.  We continually delivered builds combined with regular acceptance testing and delivery.  
These micro-prototypes were used as building blocks to quickly build new services on our existing model layer.  
We were able to rapidly build other services associated with our central prototype and build services using the micro-prototypes.  
This approach can be used while consuming any external API and provide a framework for responding in a very short period of time to requirements associated with complex requests.  
 
 [Link to the design prototype repository](https://bitbucket.org/RobertChumley/open.fda.gov-design)

 We believe the Agile process can be used and adapted to in conditions where schedules and the delivery timelines are tight while 
 leveraging the latest technologies available in cloud computing.  Our process was to lay the groundwork for an extensible solution while completely fulfilling 
 the goals of the prototype.

## Architecture:

The open.FDA.org prototype was built using a combination of technologies that form a complete open source stack of technologies.  
First, we used a Model, View Controller framework called EmberJS along with the ember server to run the application in test mode.  
Ember runs with NodeJS and can be deployed anywhere easily.  Ember uses a product called Handlebars to render html templates with common html and javascript.  
The web application display components are built with Bootstrap.js.  
Bootstrap requires jQuery which is also open source and we extended the user interface with d3.js data visualization tools.
We held prototype meetings daily.  The suggestion was presented that we should also use a NoSQL Database such as MongoDB to store some of the data while performing complex calculations.   
A caching structure such as Redis or Memcache would be used removing the need to call open.fda.org more than necessary.
This caching would greatly increasing the speed and efficiency of the entire platform.   
CloudFoundry containers can be used to inform us of when to add more resources and the resources could be loaded and allocated in consistent ways within the virtual machines.  
All configurations are stored outside the environment and the system would have unlimited scaling.

## Front-End Development:
The data from open.fda.gov and the layout of the models would work very well with data analytics tools such as R to process the data in MongoDB.    
The layout was built from the wireframes and prototypes found in the design of our use cases.  
We produced additional models that could be calculated with free components available publically on the internet such as numpy or scipy from python.

## Back-End Development:
The model layer was created to be tightly bound to the API structure of each endpoint.  
We used these layers to establish a common way of processing and binding the user interface to a well-defined model in the backing system.  
The Controller layer if made of up two components: the route and the controller.  
The route handles all the actions from the web request while the controller is responsible for connecting the action route, model and the 
necessary state for storing information in the views.  This model allows the system to be highly responsive to changes in the user interface.  
We used an automated tool to generate the backing model to the EmberJs model called called Javascript code generation tools for ember.


## Dev Ops:
The code was submitted using command line tools into our bit bucket repository.  We setup a Microsoft Azure Websites Platform-as-a-Service website and connected the bitbucket
repository to our website.  Ember contains a module which instructs Azure to auto-deploy the code to our website from the bitbucket repository for changes on the main branch.  
The unit tests are executed during this deployment and will not deploy if any unit tests fail.

We are able to fully implement horizontal scaling as well because all environment configuration is stored outside of the host operating system.  
The platform is based on Node.Js and can be deployed to a container system such as CloudFoundry from pivotal.io or Docker 
allowing us to set up containers in a host operating system such as Ubuntu.  
We would use a product like MicroBosh to deploy the solution on the hosted VM consume Azure resources from a connected terminal.  
The product is fault tolerance, load balancing, contains accurate and persistent logging and secured throughout the backend system.



## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://www.ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember server`
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://www.ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
