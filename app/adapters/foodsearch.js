 import DS from 'ember-data';
var externalHost="https://api.fda.gov/food/enforcement.json";
export default DS.Adapter.extend({

  findQuery: function(store, type, query) {
    // Do your thing here
    return Ember.$.ajax({url:externalHost,data:query,type:"GET", options:{
      crossDomain: true,
      xhrFields: {withCredentials: true},
      nounce: 12344
    }}).then(function(data) {
      var result = [];
      data.results.forEach(function(el, index){
            el['id'] = index;
            result.push(el);
      });
      return result;
    });
  }
});
