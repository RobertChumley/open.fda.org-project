 import DS from 'ember-data';
var host = "https://api.fda.gov/drug/event.json";
var externalHost="https://api.fda.gov/drug/event.json";

export default  DS.Adapter.extend({
  find: function(store, type, id) {
    // Do your thing here
     $.getJSON(externalHost);
  },

  findAll: function(store, type, sinceToken) {
    // Do your thing here
    var query;

    if (sinceToken) {
      query = { since: sinceToken };
    }

    return $.getJSON(externalHost);
  },

  findQuery: function(store, type, query) {
    // Do your thing here
    return Ember.$.ajax({url:externalHost,data:query,type:"GET", options:{
      crossDomain: true,
      xhrFields: {withCredentials: true},
      nounce: 12344
    }}).then(function(data) {
      var result = [];
      data.results.forEach(function(el, index){
            el['id'] = index;
            result.push(el);
      });
      return result;
    });
  },

  findMany: function(store, type, ids, owner) {
    return $.getJSON(externalHost,{test:"test"}, { data: { ids: ids } });
  },
});
