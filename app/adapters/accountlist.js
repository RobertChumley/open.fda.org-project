import DS from 'ember-data';

var externalHost="https://api.fda.gov/drug/event.json";

export default  DS.RESTAdapter.extend({
  host:externalHost,
  
    findAll: function(store, type, id) {
    return Ember.$.ajax(externalHost+"?search=receivedate:[20040101+TO+20150101]&count=receivedate", "GET", {
      // CORS
      crossDomain: true,
      xhrFields: {withCredentials: true},
      nounce: 12344
    }).then(function(json) {
      // Massage this to look like RESTAdapter expects.
      for(var i = 0; i < json.results.length; i++){
        json.results[i]["id"] = json.results[i]["safetyreportid"];
      }
      return { accountlist: json.results };
    });
  }
});

