import Ember from 'ember';

export default Ember.Route.extend({


	// the initial value of the `search` property
  model: function(){
    //var data = $.ajax({url:'http://'});
    return {outputlist: [] };
  },
   
  actions: {
    
    findFromDrugs: function(){
      //, count:'patient.reaction.reactionmeddrapt.exact'
      var control = this.controllerFor('drug');
      var data=this.store.find('openfda',{search:'openfda.brand_name:"' + control.get('searchTerm') + '"'}); 
      control.set('model',{outputlist : data});
      console.log(control.get('model').outputlist);
    }
  }
});
