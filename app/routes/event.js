export default Ember.Route.extend({

 queryParams: {
    ndc_code: {
      refreshModel: true
    }
  },
 
  
  // the initial value of the `search` property
  model: function(params){
    //var data = $.ajax({url:'http://'});
    
    return {data : this.store.find('event',{search:'patient.drug.openfda.product_ndc\"' + params.ndc_code + '\"' })};
  },
  
  actions: {
    alterbob: function() {
      var mod = this.modelFor('event');
      mod.vals.addObject(Ember.Object.create({vname:mod.bob}));
      Ember.set(mod, 'bob' ,'der not');
    }
  }
});
