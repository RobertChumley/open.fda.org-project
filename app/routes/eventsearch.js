export default Ember.Route.extend({
 
  model: function(){
    
    return {data:[{count: 1000,term:'hello world'}]};
  },
   
  actions: {
    
    findFromEvents: function(){
      //, count:'patient.reaction.reactionmeddrapt.exact'
      var control = this.controllerFor('eventsearch');
      var data=this.store.find('eventsearch',{
        search: control.get('selectedSearchName')  + '"' + control.get('searchTerm') + '"', count: control.get('selectedCountName'),limit:10});
      var mydata = {data : data}; 
      control.set('model',mydata);
    }
  }
});
