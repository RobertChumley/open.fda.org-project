import Ember from 'ember';

export default Ember.Route.extend({
    model:function(){
 		return {outputlist: [] };
    },
    actions: {
		findFromFood: function(){
	      //, count:'patient.reaction.reactionmeddrapt.exact'
	      var control = this.controllerFor('foodsearch');
	      var data=this.store.find('foodsearch',{search:'recalling_firm:"' + control.get('searchTerm') + '"'}); 
	      control.set('model',{outputlist : data});
	    }
	}
});
