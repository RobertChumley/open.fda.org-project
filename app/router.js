import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route("accountlist", function(){
        this.route("list", { path: "/accountlist" });
  });
  this.route("event", function(){
        this.route("list", { path: "/event" });
  });
  this.route("eventsearch", function(){
        this.route("list", { path: "/eventsearch" });
  });
  this.route('drug', function(){
      this.route("list", { path: "/drug" });
  });
  this.route('foodsearch', function(){
      this.route("list", { path: "/food" });
  });
  this.route('help', function(){
        this.route("list", { path: "/help" });
  });
  this.route('welcome');
  
});

export default Router;
