import Ember from 'ember';
import ColumnDefinition from 'ember-table/models/column-definition';


export default Ember.Controller.extend({
  // the initial value of the `search` property
  queryParams: ['ndc_code'],
  ndc_code: null,

  list:function(){

  },
 
  searchNames: [
          {id:'patient.drug.openfda.pharm_class_epc',label:'Pharmacy Class'},
          {id:'patient.drug.openfda.manufacturer_name',label:'Manufacturer'},
          {id:'patient.reaction.reactionmeddrapt',label:'Adverse Reaction'}],
  countNames:[
          {id:'patient.reaction.reactionmeddrapt.exact',label:'Adverse Reactions'},
          {id:'patient.drug.openfda.manufacturer_name.exact',label:'Manufacturer'},
          {id:'patient.patientonsetage',label:'Onset Age'} ],
  selectedSearchName:'',
  selectedCountName:'',
  searchTerm: '',

  search: '',
  bob: '',
  
  actions: {
    query: function() {
      // the current value of the text field
      var query = this.get('search');
      this.transitionToRoute('search', { query: query });
    },
    list: function(){

    } 
  },
   tableColumns: Ember.computed(function() {
    var termColumn = ColumnDefinition.create({
      savedWidth: 150,
      textAlign: 'text-align-left',
      headerCellName: 'Term',
      getCellContent: function(row) {
        return row.get('term');
      }
    });
    var countColumn = ColumnDefinition.create({
      savedWidth: 100,
      headerCellName: 'Count',
      getCellContent: function(row) {
        return row.get('count');
      }
    });
    return [termColumn, countColumn];
  })
});