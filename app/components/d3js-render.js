var G3 = {
	xScale: null,
	yScale: null
};

export default Ember.Component.extend({
 dataContent: [],
 graph: null,
 attributeBindings: ['dataContent'],
 loaddata: function() {
 	var data =[];
 	var dataCounts=[];
 	var max = 0;
 	this.dataContent.forEach(function(el, index){
 		if(el.get('count') > max)
 			max = el.get('count');
 		dataCounts.push(el.get('count'));
        data.push({count:el.get('count'),term:el.get('term')});
    });  
     
    /*var graphData = d3.select("#svg")
	  		.selectAll("div")
	    	.data(data);
	// enter selection
	graphData.enter().append("div");
	
    var pointColour = d3.scale.category10();
	// update selection
	graphData
	    .style('background',function(d, i) {
	    	return pointColour(i);
	    })
	    .style("width", function(d) {
	    	return ((d.count / max) *100)  + "%"; 
	    })
	    .attr("class", "bar-item")
	    .text(function(d) { 
	    	return d.term; 
	    });

	// exit selection 
	graphData.exit().remove();
*/
	d3.select("svg").remove();

    var chart = d3.select("#svg")
   .append("svg:svg")
     .attr("class", "chart")
     .attr("width", 580)
     .attr("height", 30 * dataCounts.length)
	 .append("svg:g")
     .attr("transform", "translate(10,15)");


	var x = d3.scale.linear()
     .domain([0, d3.max(dataCounts)])
     .range([0, 580]);

    var y = d3.scale.ordinal()
     	.domain(dataCounts)
     	.rangeBands([0, 290]);

    

     /*.text(function(d) { 
	    	return d.term; 
	  });*/

	chart.selectAll("line")
     .data(x.ticks(10))
     .enter().append("svg:line")
     .attr("x1", x)
     .attr("x2", x)
     .attr("y1", 0)
     .attr("y2",  30 * dataCounts.length)
     .attr("stroke", "#ccc");

var tip = d3.tip()
	  .attr('class', 'd3-tip')
	  .offset([0, 10])
	  .html(function(d) {
	    return "<strong>Count:</strong> <span style='color:red'>" + d + "</span>";
	  });
	
    chart.selectAll("rect") 
     .data(dataCounts)
      .enter().append("svg:rect")
      .attr("y", y)
      .attr("width", x)
      .attr("height", y.rangeBand())
	  .on('mouseover', tip.show)
      .on('mouseout', tip.hide);
    


	chart.call(tip);

    chart.selectAll("text")
     .data(data)
     .enter().append("svg:text")
     .attr("x", function(d) { return x(d.count); })
     .attr("y", function(d) { return y(d.count) + y.rangeBand() / 2; })
     .attr("dx", -3) // padding-right
     .attr("dy", ".35em") // vertical-align: middle
     .attr("text-anchor", "end") // text-align: right
     .attr('fill','white')
     .attr('font-size','10px')
     
     .text(function(d) { 
	    	return d.term; 
	  });

chart.selectAll("text.rule")
     .data(x.ticks(10))
     .enter().append("svg:text")
     .attr("class", "rule")
     .attr("x", x)
     .attr("y", 0)
     .attr("dy", -3)
     .attr("text-anchor", "middle")
     .text(String);


	chart.append("svg:line")
    .attr("y1", 0)
     .attr("y2", 320)
     .attr("stroke", "#000");


	


	/*var graphData = d3.select("#svg")
	  		.selectAll("div")
	    	.data(data);
	// enter selection
	graphData.enter().append("div");
    var pointColour = d3.scale.category10();
	// update selection
	graphData
	    .style('background',function(d, i) {
	    	return pointColour(i);
	    })
	    .style("width", function(d) {
	    	return ((d.count / max) *100)  + "%"; 
	    })
	    .attr("class", "bar-item")
	    .text(function(d) { 
	    	return d.term; 
	    });

	// exit selection
	graphData.exit().remove();
    */
  }.observes('dataContent.[]')
}); 