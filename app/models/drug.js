import DS from 'ember-data';

export default DS.Model.extend({
	spl_id: DS.attr(),  
	product_ndc: DS.attr(),  
	is_original_packager: DS.attr(),  
	route: DS.attr(),  
	substance_name: DS.attr(),  
	spl_set_id: DS.attr(),  
	package_ndc: DS.attr(),  
	product_type: DS.attr(),  
	generic_name: DS.attr(),  
	manufacturer_name: DS.attr(),  
	brand_name: DS.attr(),  
	application_number: DS.attr()  
});