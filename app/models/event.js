export default DS.Model.extend({
	
  safetyreportid: DS.attr('string'),
  fulfillexpeditecriteria: DS.attr('string'),
  receiver:  DS.attr(),
  receivedateformat: DS.attr(),
  primarysource: DS.attr(),
  receivedate: DS.attr(),
  seriousnessother: DS.attr(),
  sender: DS.attr(),
  patient: DS.attr()
});