import DS from 'ember-data';

export default DS.Model.extend({
 recall_number: DS.attr(),
 reason_for_recall: DS.attr(),
 status: DS.attr(),
 distribution_pattern: DS.attr(),
 product_quantity: DS.attr(),
 recall_initiation_date: DS.attr(),
 state: DS.attr(),
 event_id: DS.attr(),
 product_type: DS.attr(),
 product_description: DS.attr(),
 country: DS.attr(),
 city: DS.attr(),
 recalling_firm: DS.attr(),
 report_dat: DS.attr(),
 voluntary_mandated: DS.attr(),
 classification: DS.attr(),
 code_info: DS.attr(),
 openfda: DS.attr(),
 initial_firm_notification: DS.attr()
});
