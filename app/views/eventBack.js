var G3 = {
	xScale: null,
	yScale: null
};


export default Ember.View.extend({
  name: "Bob",
  didInsertElement: function() {
    var svg = d3.select("#svg");
    var data = this.get('controller.contentData');
    var newData = data.toArray();
    this.loaddata(newData);
    /*var graphics = svg.selectAll("rect")
      .data(newData)
      .enter()
      .append("rect");
    graphics.attr("x", function(d, i) {
    	console.log(d.x);
      //return d.get('x');
      return d.x;
    })*/
 },


 
 loaddata: function(data) {
	 var xAxis =  null;
	 var yAxis =  null;
	 var bounds =  null;
	 var descriptions =  null;
	 var xAxisOptions =  null;	  
	 var datamethods = {
  	    //// RENDERING FUNCTIONS
  	    xAxis: null,
  	    yAxis: null,
  	    bounds: null,
	    updateChart: function(init,data) {
		    this.updateScales();
		    d3.select('svg g.chart')
		      .selectAll('circle')
		      .transition()
		      .duration(500)
		      .ease('quad-out')
		      .attr('cx', function(d) {
		        return isNaN(d[ this.xAxis]) ? d3.select(this).attr('cx') :  G3.xScale(d[this.xAxis]);
		      })
		      .attr('cy', function(d) {
		        return isNaN(d[ this.yAxis]) ? d3.select(this).attr('cy') :  G3.yScale(d[this.yAxis]);
		      })
		      .attr('r', function(d) {
		        return isNaN(d[ this.xAxis]) || isNaN(d[ this.yAxis]) ? 0 : 12;
		      });

		    // Also update the axes
		    d3.select('#xAxis')
		      .transition()
		      .call( this.makeXAxis);

		    d3.select('#yAxis')
		      .transition()
		      .call( this.makeYAxis);

		    // Update axis labels
		    /*d3.select('#xLabel')
		      .text( this.descriptions[ this.xAxis]);   
		*/
		    var lXAxis = this.xAxis;   
		    // Update correlation
		    var xArray = _.map(data, function(d) {
		    	return d[lXAxis];
		    });
		    var lYAxis = this.yAxis;
		    var yArray = _.map(data, function(d) {return d[lYAxis];});
		    var c = this.getCorrelation(xArray, yArray);
		    var x1 =  G3.xScale.domain()[0], y1 = c.m * x1 + c.b;
		    var x2 =  G3.xScale.domain()[1], y2 = c.m * x2 + c.b;

			    // Fade in
			  /*d3.select('#bestfit')
			      .style('opacity', 0)
			      .attr({'x1':  G3.xScale(x1), 'y1':  G3.yScale(y1), 'x2':  G3.xScale(x2), 'y2':  G3.yScale(y2)})
			      .transition()
			      .duration(1500)
			      .style('opacity', 1);
			     */
		  },

		  updateScales: function() {
		    G3.xScale = d3.scale.linear()
		                    .domain([this.bounds[this.xAxis].min, this.bounds[this.xAxis].max])
		                    .range([20, 480]);

		    G3.yScale = d3.scale.linear()
		                    .domain([this.bounds[this.yAxis].min, this.bounds[this.yAxis].max])
		                    .range([450, 100]);     
		  },
		  makeXAxis: function(s) { 
		    s.call(d3.svg.axis()
		      .scale(G3.xScale)
		      .orient("bottom"));
		  },
		  makeYAxis: function(s) { 
		    s.call(d3.svg.axis()
		      .scale(G3.yScale)
		      .orient("left"));
		  },

		  updateMenus: function() {
		    d3.select('#x-axis-menu')
		      .selectAll('li')
		      .classed('selected', function(d) {
		        return d ===  this.xAxis;
		      });
		    d3.select('#y-axis-menu')
		      .selectAll('li')
		      .classed('selected', function(d) {
		        return d ===  this.yAxis;
		    });
		  },// HELPERS
		 parseData: function(d) {
		  var keys = _.keys(d[0]);
		  return _.map(d, function(d) {
		    var o = {};
		    _.each(keys, function(k) {
		      if( k == 'Country' )
		        o[k] = d[k];
		      else
		        o[k] = parseFloat(d[k]);
		    });
		    return o;
		  });
		},

		getBounds: function(d, paddingFactor) {
		  // Find min and maxes (for the scales)
		  paddingFactor = typeof paddingFactor !== 'undefined' ? paddingFactor : 1;

		  var keys = _.keys(d[0]), b = {};
		  _.each(keys, function(k) {
		    b[k] = {};
		    _.each(d, function(d) {
		      if(isNaN(d[k]))
		        return;
		      if(b[k].min === undefined || d[k] < b[k].min)
		        b[k].min = d[k];
		      if(b[k].max === undefined || d[k] > b[k].max)
		        b[k].max = d[k];
		    });
		    b[k].max > 0 ? b[k].max *= paddingFactor : b[k].max /= paddingFactor;
		    b[k].min > 0 ? b[k].min /= paddingFactor : b[k].min *= paddingFactor;
		  });
		  return b;
		},

		 getCorrelation: function(xArray, yArray) {
			  function sum(m, v) {return m + v;}
			  function sumSquares(m, v) {return m + v * v;}
			  function filterNaN(m, v, i) {isNaN(v) ? null : m.push(i); return m;}

			  // clean the data (because we know that some values are missing)
			  var xNaN = _.reduce(xArray, filterNaN , []);
			  var yNaN = _.reduce(yArray, filterNaN , []);
			  var include = _.intersection(xNaN, yNaN);
			  var fX = _.map(include, function(d) {return xArray[d];});
			  var fY = _.map(include, function(d) {return yArray[d];});

			  var sumX = _.reduce(fX, sum, 0);
			  var sumY = _.reduce(fY, sum, 0);
			  var sumX2 = _.reduce(fX, sumSquares, 0);
			  var sumY2 = _.reduce(fY, sumSquares, 0);
			  var sumXY = _.reduce(fX, function(m, v, i) {return m + v * fY[i];}, 0);

			  var n = fX.length;
			  var ntor = ( ( sumXY ) - ( sumX * sumY / n) );
			  var dtorX = sumX2 - ( sumX * sumX / n);
			  var dtorY = sumY2 - ( sumY * sumY / n);
			 
			  var r = ntor / (Math.sqrt( dtorX * dtorY )); // Pearson ( http://www.stat.wmich.edu/s216/book/node122.html )
			  var m = ntor / dtorX; // y = mx + b
			  var b = ( sumY - m * sumX ) / n;

			  // console.log(r, m, b);
			  return {r: r, m: m, b: b};
		}
 	  };
	  xAxis = 'x', yAxis = 'y';
	  datamethods.xAxis = xAxis;
	  datamethods.yAxis = yAxis;
	  
	  xAxisOptions = ["GDP", "Equality", "Food consumption", "Alcohol consumption", "Energy consumption", "Family", "Working hours", "Work income", "Health spending", "Military spending"]
	  // var yAxisOptions = ["Well-being"];
	  descriptions = {
	    "GDP" : "GDP per person (US$)",
	    "Energy consumption" : "Residential electricity use (kWh per year per person)",
	    "Equality" : "Equality (based on GINI index) (0 = low equality, 100 = high equality)",
	    "Work income" : "Hourly pay per person (US$)",
	    "Food consumption": "Food supply (kCal per day per person)",
	    "Family" : "Fertility (children per women)",
	    "Alcohol consumption" : "Alcohol consumption (litres of pure alchohol per year per person)",
	    "Working hours" : "Average working hours per week per person",
	    "Military spending" : "Military spending (% of GDP)",
	    "Health spending" : "Government health spending (% of government spend)"
	  };

	  var keys = _.keys(data[0]);
	  datamethods.bounds = datamethods.getBounds(data, 1);

	  // SVG AND D3 STUFF
	  var svg = d3.select("#chart")
	    .append("svg")
	    .attr("width", 600)
	    .attr("height", 440);
	   
	  svg.append('g')
	    .classed('chart', true)
	    .attr('transform', 'translate(80, -60)');

	  // Build menus
	  /*d3.select('#x-axis-menu')
	    .selectAll('li')
	    .data(xAxisOptions)
	    .enter()
	    .append('li')
	    .text(function(d) {return d;})
	    .classed('selected', function(d) {
	      return d === xAxis;
	    })
	    .on('click', function(d) {
	      console.log(d);
	      datamethods.xAxis = d;
	      datamethods.updateChart();
	      datamethods.updateMenus();
	    });
	*/
	  // d3.select('#y-axis-menu')
	  //   .selectAll('li')
	  //   .data(yAxisOptions)
	  //   .enter()
	  //   .append('li')
	  //   .text(function(d) {return d;})
	  //   .classed('selected', function(d) {
	  //     return d === yAxis;
	  //   })
	  //   .on('click', function(d) {
	  //     yAxis = d;
	  //     updateChart();
	  //     updateMenus();
	  //   });

	  // Country name
	  d3.select('svg g.chart')
	    .append('text')
	    .attr({'id': 'countryLabel', 'x': 0, 'y': 170})
	    .style({'font-size': '80px', 'font-weight': 'bold', 'fill': '#555'});

	  // Best fit line (to appear behind points)
	  d3.select('svg g.chart')
	    .append('line')
	    .attr('id', 'bestfit');

	  // Axis labels
	  /*d3.select('svg g.chart') 
	    .append('text')
	    .attr({'id': 'xLabel', 'x': 100, 'y': 300, 'text-anchor': 'left'})
	    .text(descriptions[xAxis]);
*/
	  d3.select('svg g.chart')
	    .append('text')
	    .attr('transform', 'translate(-60, 330)rotate(-90)')
	    .attr({'id': 'yLabel', 'text-anchor': 'middle'})
	    .text('Example (scale of 0-10)');

	  // Render points
	  datamethods.updateScales();

	  var pointColour = d3.scale.category20b();
	  d3.select('svg g.chart')
	    .selectAll('circle')
	    .data(data)
	    .enter()
	    .append('svg:circle')  
	    .attr("r", "10px")
	    .attr('cx', function(d) {
	      return d[xAxis];
	    })
	    .attr('cy', function(d) {
	      return d[yAxis];
	    })
	    .attr('fill', function(d, i) {
	    	return pointColour(i);
	    })
	    .style('cursor', 'pointer')
	    .on('mouseover', function(d) {  
	      d3.select('svg g.chart #countryLabel')
	        .text(d.Country)
	        .transition()
	        .style('opacity', 1);
	    })
	    .on('mouseout', function(d) {
	      d3.select('svg g.chart #countryLabel')
	        .transition()
	        .duration(1500)
	        .style('opacity', 0);
	    });
	

        var lineGen = d3.svg.line()
		  .x(function(d) {
		    return G3.xScale(d.x);
		  })
		  .y(function(d) {
		    return G3.yScale(d.y);
		  }).interpolate("basis");
 		d3.select('svg g.chart')
		.append('svg:path')
		  .attr('d', lineGen(data))
		  .attr('stroke', 'green')
		  .attr('stroke-width', 2)
		  .attr('fill', 'none');

	   datamethods.updateChart(true,data);
	   datamethods.updateMenus();
      
	  // Render axes
	  d3.select('svg g.chart')
	    .append("g")
	    .attr('transform', 'translate(0, 465)') 
	    .attr('id', 'xAxis')
	    .call(datamethods.makeXAxis);

	  d3.select('svg g.chart')
	    .append("g")
	    .attr('id', 'yAxis')
	    .attr('transform', 'translate(-10, 0)')
	    .call(datamethods.makeYAxis);	 
  }
});